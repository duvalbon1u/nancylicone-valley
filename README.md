Bienvenu sur le dpot git de la team Nancylicone Valley

Le site se trouve dans site/

Pour run le site :

    Copier le .env.example ver .env
    Modifier le .env pour que DB_DATABASE DB_USERNAME et DB_PASSWORD correspondent à votre configuration mysql
    Lancer php artisan migrate:fresh --seed pour générer les tables et les données
    Lancer php artisan serve pour lancer un serveur
    Ouvrir localhost:8000/
    
Bonne navigation ! 
