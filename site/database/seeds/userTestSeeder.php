<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Todolists
        DB::table('todo_lists')->insert([
            'title' => "Protocole médical",
            'checked' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('todo_lists')->insert([
            'title' => "Protocole technique",
            'checked' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('todo_lists')->insert([
            'title' => "Bilan santé",
            'checked' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('todo_lists')->insert([
            'title' => "Entretien des matériels",
            'checked' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('todo_lists')->insert([
            'title' => "Vérifications et maintenance des systèmes de survie",
            'checked' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('todo_lists')->insert([
            'title' => "Contrôle du camp",
            'checked' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('todo_lists')->insert([
            'title' => "Exercices sportifs",
            'checked' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        
        
        // Task
        DB::table('tasks')->insert([
            'task_title' => "Vérification du pouls",
            'checked' => false,
            'description' => 'Vérifier le pouls à l\'aide de la montre connectée ou des équipements médicaux au camp.',
            'id_todo' => 1,
            'id_equipement' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Dernier repas",
            'checked' => false,
            'description' => 'Note du dernier repas pris.',
            'id_todo' => 1,
            'id_equipement' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Dernière boisson",
            'checked' => false,
            'description' => 'Note de la dernière boisson prise.',
            'id_todo' => 1,
            'id_equipement' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Vérification du bon fonctionnement du robot",
            'checked' => false,
            'description' => 'Vérifier le bon fonctionnement du robot conformément au protocole communiqué.',
            'id_todo' => 2,
            'id_equipement' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Vérification du bon fonctionnement des panneaux solaires",
            'checked' => false,
            'description' => 'Vérifier le bon fonctionnement des panneaux solaires et de leur production conformément au protocole communiqué.',
            'id_todo' => 2,
            'id_equipement' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Mesure du poids",
            'checked' => true,
            'description' => 'Mesurer le poids.',
            'id_todo' => 3,
            'id_equipement' => 4,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Mesure de la taille",
            'checked' => true,
            'description' => 'Mesurer la taille.',
            'id_todo' => 3,
            'id_equipement' => 5,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Entretien des panneaux solaires",
            'checked' => false,
            'description' => 'Nettoyer les panneaux solaires.',
            'id_todo' => 4,
            'id_equipement' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Entretien du robot",
            'checked' => false,
            'description' => 'Nettoyer le robot et maintenance du robot.',
            'id_todo' => 4,
            'id_equipement' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'task_title' => "Verification des rations d\'eau et de nourritures restantes",
            'checked' => true,
            'description' => 'Verification des rations restantes et mesure des quantités par rapports aux stocks prévus',
            'id_todo' => 5,
            'id_equipement' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
		DB::table('tasks')->insert([
            'task_title' => "Vérification des composants du systeme de survie",
            'checked' => true,
            'description' => 'Lancement des analyses d\'états du systeme afin de détécter d\'éventuels annomalies',
            'id_todo' => 5,
            'id_equipement' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
		DB::table('tasks')->insert([
            'task_title' => "Vérification de la tente",
            'checked' => false,
            'description' => 'Vérification de l\'herméticité de la tente afin d\'éviter tout risque d\'intrusion animal ou végétal indésirée',
            'id_todo' => 6,
            'id_equipement' => 6,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
		DB::table('tasks')->insert([
            'task_title' => "Vérification des installations du camp",
            'checked' => false,
            'description' => 'Vérification des installations du camp afin de reprérer tout risque d\'anomalies dut au climat ou aux intempéries',
            'id_todo' => 6,
            'id_equipement' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        
        
        
        // Equipements
        DB::table('equipements')->insert([
            'name' => "Montre connectée",
            'carac' => '78%',
            'type' => 'montre',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('equipements')->insert([
            'name' => "Robot",
            'carac' => '85%',
            'type' => 'drone',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('equipements')->insert([
            'name' => "Panneaux solaires",
            'carac' => '95%',
            'type' => 'pann',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('equipements')->insert([
            'name' => "Plastron",
            'carac' => '',
            'type' => 'torse',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('equipements')->insert([
            'name' => "Jambière",
            'carac' => '',
            'type' => 'jambe',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('equipements')->insert([
            'name' => "Tente",
            'carac' => '',
            'type' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        
        // Stocks
        DB::table('stocks')->insert([
            'name' => "Viande de Boeuf",
            'quantity' => 750,
            'id_type' => 1,
            'unity' => 'g',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Eau",
            'quantity' => 50,
            'id_type' => 2,
            'unity' => 'L',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Carotte",
            'quantity' => 3,
            'id_type' => 1,
            'unity' => 'Kg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Pomme de terre",
            'quantity' => 5,
            'id_type' => 1,
            'unity' => 'Kg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Haricots verts",
            'quantity' => 2,
            'id_type' => 1,
            'unity' => 'Kg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Riz",
            'quantity' => 7,
            'id_type' => 1,
            'unity' => 'Kg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Riz",
            'quantity' => 7,
            'id_type' => 1,
            'unity' => 'Kg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Doliprane",
            'quantity' => 2,
            'id_type' => 3,
            'unity' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('stocks')->insert([
            'name' => "Essence",
            'quantity' => 20,
            'id_type' => 4,
            'unity' => 'L',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        // types
        DB::table('types')->insert([
            'name' => "Nourriture",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('types')->insert([
            'name' => "Liquide",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('types')->insert([
            'name' => "Médicament",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('types')->insert([
            'name' => "Carburant",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        DB::table('planning')->insert([
            'horaire' => "08:00:00",
            'id_todo' => "1",
        ]);
        DB::table('planning')->insert([
            'horaire' => "08:30:00",
            'id_todo' => "3",
        ]);
        DB::table('planning')->insert([
            'horaire' => "08:45:00",
            'id_todo' => "4",
        ]);
        DB::table('planning')->insert([
            'horaire' => "09:00:00",
            'id_todo' => "5",
        ]);
    }
}
