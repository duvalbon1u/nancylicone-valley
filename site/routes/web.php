<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('root');

Route::get('/excursion', function () {
    return view('excursion');
})->name('excursion');

Route::get('/menu', function () {
    return view('menu');
})->name('menu');

Route::get('stocks/list','StockController@list')->name('stocks.list');
Route::resource('stocks', 'StockController');
Route::get('equipement/list','EquipementController@list')->name('equipement.list');
Route::resource('equipement', 'EquipementController');
Route::get('tableau-de-bord', 'TableauDeBordController@afficher')->name('tableau-de-bord');
Route::resource('tasks', 'TasksController');
Route::resource('todo-list', 'TodoListController');
Route::resource('types', 'TypeController');
Route::resource('planning', 'PlanningController');
Route::resource('data','DataController');

Route::get('/home', 'HomeController@index')->name('home');
