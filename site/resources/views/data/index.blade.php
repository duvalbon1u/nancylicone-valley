@extends('default')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-tasks"></i>
            Gérer les données personnelles</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Poids</th>
                <th>Taille</th>
                <th>Poul</th>
                <th>Dernier repos</th>
                <th>Dernier repas</th>
                <th>Dernier boisson</th>
                <th>Action</th>
                </thead>
                <tbody>
                @foreach($datas as $d)
                    <tr style="text-align: center">
                        <td>{{$d->weight}}</td>
                        <td>{{$d->height}}</td>
                        <td>{{$d->beat}}</td>
                        <td>{{$d->last_time_sleep}}</td>
                        <td>{{$d->last_time_eat}}</td>
                        <td>{{$d->last_time_drink}}</td>
                        <td>
                            <a href="{{{ route('data.edit', $d) }}}" class="btn btn-primary">Editer</a>
                            <form action="{{{ route('data.destroy', $d) }}}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('data.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
