@extends('default')

@section('content')
    <h1>Editer</h1>

    <div class="card card-default">
        <div class="card-header">Modifier les informations utilisateur</div>
        <div class="card-body">

            <form action="{{route('data.store')}}" method="POST">
                @csrf

                <div class="form-group">

                    <div class="col-md-6">
                        <label for="weight" class='col-md-4 control-label'>Poids</label>
                        <input type="text" class="form-control" id="title" name="weight" >
                    </div>
                    <div class="col-md-6">
                        <label for="height" class='col-md-4 control-label'>Taille</label>
                        <input type="text" class="form-control" id="title" name="height">
                    </div>
                    <div class="col-md-6">
                        <label for="beat" class='col-md-4 control-label'>Pouls</label>
                        <input type="text" class="form-control" id="title" name="beat">
                    </div>
                    <div class="col-md-6">
                        <label for="last_time_sleep" class='col-md-4 control-label'>Dernière nuit</label>
                        <input type="text" class="form-control" id="title" name="last_time_sleep" value="{{$datenow}}">
                    </div>
                    <div class="col-md-6">
                        <label for="last_time_eat" class='col-md-4 control-label'>Dernier repas</label>
                        <input type="text" class="form-control" id="title" name="last_time_eat" value="{{$datenow}}">
                    </div>
                    <div class="col-md-6">
                        <label for="last_time_drink" class='col-md-4 control-label'>Dernière boisson</label>
                        <input type="text" class="form-control" id="title" name="last_time_drink" value="{{$datenow}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
