@extends('default')

@section('content')
    <h1>Editer</h1>

    <div class="card card-default">
        <div class="card-header">Modifier les informations utilisateur</div>
        <div class="card-body">

            <form action="{{route('data.update', $data->id)}}" method="POST">
                @csrf
                @method('PUT')
                
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="weight" class='col-md-4 control-label'>Poids</label>
                        <input type="text" class="form-control" id="title" name="weight" value="{{$data->weight}}">
                    </div>
                    <div class="col-md-6">
                        <label for="height" class='col-md-4 control-label'>Taille</label>
                        <input type="text" class="form-control" id="title" name="height" value="{{$data->height}}">
                    </div>
                    <div class="col-md-6">
                        <label for="beat" class='col-md-4 control-label'>Pouls</label>
                        <input type="text" class="form-control" id="title" name="beat" value="{{$data->beat}}">
                    </div>
                    <div class="col-md-6">
                        <label for="last_time_sleep" class='col-md-4 control-label'>Dernière nuit</label>
                        <input type="text" class="form-control" id="title" name="last_time_sleep" value="{{$data->last_time_sleep}}">
                    </div>
                    <div class="col-md-6">
                        <label for="last_time_eat" class='col-md-4 control-label'>Dernier repas</label>
                        <input type="text" class="form-control" id="title" name="last_time_eat" value="{{$data->last_time_eat}}">
                    </div>
                    <div class="col-md-6">
                        <label for="last_time_drink" class='col-md-4 control-label'>Dernière boisson</label>
                        <input type="text" class="form-control" id="title" name="last_time_drink" value="{{$data->last_time_drink}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
