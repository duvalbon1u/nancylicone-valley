@extends('default')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-shopping-cart"></i>
            Gérer les types</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Nom</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($types as $type)
                    <tr>
                        <td>{{{$type->name}}}</td>
                        <td>
                            <a href="{{{ route('types.edit', $type) }}}" class="btn btn-primary">Editer</a>
                            <a href="{{{ route('types.show', $type) }}}" class="btn btn-primary">Afficher</a>
                            <form action="{{{ route('types.destroy', $type) }}}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('types.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
