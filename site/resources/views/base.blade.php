t<!DOCTYPE html>

<html lang="french">
<head>
      <meta charset="utf-8">
      <link href="./css/app.css" rel="stylesheet" />
      <title>Futurexplor</title>
</head>

<body>
      <header>
            @section('header')
            <p>Header</p>
            @show
      </header>
      @section('content')
      <div class="container">
            <div class="row">
                  <div class="col-2">
            <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non orci in odio porttitor ultrices. Integer porta tincidunt ipsum in suscipit. Sed risus velit, rhoncus accumsan interdum id, rhoncus sed ipsum. Nulla id egestas augue, et aliquet nibh. Mauris felis mi, vestibulum id sagittis at, accumsan eu nisi. Donec nisl eros, suscipit eu ante in, finibus tristique sem. Curabitur dui neque, porta ac rhoncus sit amet, faucibus vel lacus. Aenean non libero lectus. Duis vehicula metus ut ultricies viverra.
            </p>
                  </div>

                  <div class="col-6">
            <p>

                  Nam quis sapien a dolor gravida viverra in ut arcu. Ut placerat gravida metus sed commodo. Vivamus venenatis, velit malesuada tristique elementum, velit purus vehicula tortor, in lobortis leo tellus id dolor. Praesent semper sapien ultricies, auctor tortor id, tempus arcu. Curabitur pretium, mauris vel consequat molestie, nibh ipsum sagittis velit, eget fringilla erat arcu eu turpis. Quisque volutpat ex sit amet tellus bibendum, non semper lectus lobortis. Quisque nec vulputate massa.
            </p>
      </div>
<div class="col">
            <p>
                  Sed odio sem, molestie eu turpis vitae, lobortis volutpat lectus. Suspendisse potenti. Donec vel urna eget dolor tempor laoreet sit amet sit amet quam. Aliquam a mollis enim, ut tincidunt nulla. Duis porta molestie eros eu cursus. Morbi gravida in eros vitae consequat. In in pretium ipsum. Proin quis metus nec diam lacinia tincidunt eget sit amet velit.
            </p>
      </div>
      </div>
@show
      <footer>

      </footer>

</body>
</html>
