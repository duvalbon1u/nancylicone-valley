@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Nouvel équipement</div>
        <div class="card-body">

            <form action="{{route('equipement.store')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="name" class='col-md-4 control-label'>Nom</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="col-md-6">
                        <label for="carac" class='col-md-4 control-label'>Caractéristique</label>
                        <input type="text" class="form-control" id="carac" name="carac">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Nouvel équipement
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
