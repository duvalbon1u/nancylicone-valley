@extends('default')
@section('content')
<script
      src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"></script>
<script src="/js/equipements.js"></script>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <h1> Equipements </h1>
            <div class="accordion" id="accordionExample">
            @foreach($equipements as $var)
                <div class="card">
                <div class="card-header" id="{{$var['id']}}">
                  <h5 class="mb-0">
                    <button id ="{{$var['type']}}" class="btn btn-link collapsed text-body" type="button" data-toggle="collapse" data-target={{"#collapse".$var['id']}} aria-expanded="true" aria-controls="collapseOne">
                       {{$var['name']}}
                    </button>
                  </h5>
                </div>
                <div id={{"collapse".$var['id']}} class="collapse text-left " aria-labelledby="" data-parent="#accordionExample">
                  <div class="card-body">
                    <p>Dernière vérification  {{\Carbon\Carbon::parse($var['updated_at'])->format("\l\\e d/m/Y à H:i:s")}}</p>
                      @if($var['carac'])
                        <p>Batterie : {{$var['carac']}}</p>
                      @endif
                  </div>
                </div>
              </div>
            @endforeach
            </div>
        </div>
        <div class="col-lg-6">
            <h1> Aperçu </h1>
            <img id ="aperc" src="/img/Bonhomme.png">
        </div>
    </div>
</div>
@endsection
