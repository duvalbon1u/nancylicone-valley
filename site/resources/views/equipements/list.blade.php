@extends('default')
@section("title")
    Liste des équipements
@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-shopping-cart"></i>
            Gérer les équipements</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Nom</th>
                <th>Dernière vérification</th>
                <th>Caractéristique</th>
                <th>Type</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($equipements as $equipement)
                    <tr style="text-align: center">
                        <td>{{{$equipement->name}}}</td>
                        <td>{{$equipement->updated_at}}</td>
                        <td>{{$equipement->carac}}</td>
                        <td>{{$equipement->type}}</td>
                        <td><a href="{{{ route('equipement.edit', $equipement) }}}" class="btn btn-primary">Editer</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('equipement.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
