@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Nouveau planning</div>
        <div class="card-body">

            <form action="{{route('planning.store')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="horaire" class='col-md-4 control-label'>Horaire</label>
                        <input type="time" class="form-control" id="task_title" name="task_title">
                    </div>
                    <div class="col-md-6">
                        <label for="id_todo" class='col-md-4 control-label'>Todo-List</label>
                        <select class="form-control" id="id_todo" name="id_todo">
                            @foreach($tdls as $tdl)
                            <option value="{{$tdl->id}}">{{$tdl->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Nouveau Planning
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection