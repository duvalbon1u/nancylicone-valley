@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Nouveau Planning</div>
        <div class="card-body">

            <form action="{{route('planning.update', $planning->id)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="horaire" class='col-md-4 control-label'>Horaire</label>
                        <input type="time" class="form-control" id="task_title" name="task_title" value="">
                    </div>
                    <div class="col-md-6">
                        <label for="id_todo" class='col-md-4 control-label'>Todo-List</label>
                        <select class="form-control" id="id_todo" name="id_todo">
                            @foreach($tdl as $td)
                                <option value="{{ $td->id }}" selected>{{$td->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Nouveau Planning
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection

