@extends('default')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-tasks"></i>
            Gérer les stocks</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Horaire</th>
                <th>Todo-List</th>
                </thead>
                <tbody>
                @foreach($plannings as $plan)
                    <tr>
                        <td>{{{$plan->horaire}}}</td>
                        <td>{{$plan->id_todo}}</td>
                        <td>
                            <a href="{{{ route('planning.edit', $plan) }}}" class="btn btn-primary">Editer</a>
                            <a href="{{{ route('planning.edit', $plan) }}}" class="btn btn-primary">Afficher</a>
                            <form action="{{{ route('planning.destroy', $plan) }}}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('planning.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
