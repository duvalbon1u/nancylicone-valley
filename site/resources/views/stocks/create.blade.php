@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Nouveau type</div>
        <div class="card-body">

            <form action="{{route('stocks.store')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="name" class='col-md-4 control-label'>Nom</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="col-md-6">
                        <label for="quantity" class='col-md-4 control-label'>Quantité</label>
                        <input type="number" class="form-control" id="quantity" name="quantity">
                    </div>
                    <div class="col-md-6">
                        <label for="id_type" class='col-md-4 control-label'>Type</label>
                        <select class="form-control" id="id_type" name="id_type">
                            @foreach($types as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="unity" class='col-md-4 control-label'>Unité</label>
                        <input type="text" class="form-control" id="unity" name="unity">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Nouveau type
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
