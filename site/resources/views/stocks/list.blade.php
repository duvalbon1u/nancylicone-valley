@extends('default')
@section("title")
    Liste des stocks
@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-shopping-cart"></i>
            Gérer les stocks</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Nom</th>
                <th>Quantité</th>
                <th>Type</th>
                </thead>
                <tbody>
                @foreach($stocks as $stock)
                    <tr style="text-align: center">
                        <td>{{{$stock->name}}}</td>
                        <td>{{$stock->quantity}} {{$stock->unity}}</td>
                        <td>{{$types->find($stock->id_type)['name']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('stocks.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
