@extends('default')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-shopping-cart"></i>
            Gérer les stocks</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Nom</th>
                <th>Quantité</th>
                <th>Type</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($stocks as $stock)
                    <tr>
                        <td>{{{$stock->name}}}</td>
                        <td>{{$stock->quantity}} {{$stock->unity}}</td>
                        <td>{{$types->find($stock->id_type)['name']}}</td>
                        <td>
                            <a href="{{{ route('stocks.edit', $stock) }}}" class="btn btn-primary">Editer</a>
                            <a href="{{{ route('stocks.edit', $stock) }}}" class="btn btn-primary">Afficher</a>
                            <form action="{{{ route('stocks.destroy', $stock) }}}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('stocks.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
