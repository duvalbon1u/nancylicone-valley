@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Passer une commande</div>
        <div class="card-body">

            <form action="{{route('stocks.update', $stock->id)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="name" class='col-md-4 control-label'>Nom</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$stock->name}}">
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="quantity" class='col-md-4 control-label'>Quantité</label>
                        <input type="number" class="form-control" id="quantity" name="quantity" value="{{$stock->quantity}}">
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-md-6">
                        <label for="id_type" class='col-md-4 control-label'>Type</label>
                        <select class="form-control" id="id_type" name="id_type">
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="unity" class='col-md-4 control-label'>Unitée</label>
                        <input type="text" class="form-control" id="unity" name="unity" value="{{$stock->unity}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Nouveau stock
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
