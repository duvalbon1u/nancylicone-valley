@extends('default')
@section('content')
<div class="container">
      <div class="row">
          <a class="col-3 square d-flex nolink" href="{{route('tableau-de-bord')}}">
              <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme5'>
                  <h2>Tableau de bord</h2>
              </div>
          </a>
          <a class="col-3 square d-flex nolink" href="{{route('equipement.index')}}">
              <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme5'>
                  <h2>Equipement</h2>
              </div>
          </a>
          <a class="col-3 square d-flex nolink" href="{{route('data.index')}}">
              <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme5'>
                  <h2>Gérer les signaux vitaux</h2>
              </div>
          </a>
            <a class="col-3 square d-flex nolink" href="{{route('excursion')}}">
                <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme5'>
                  <h2>Aller en expédition</h2>
              </div>
            </a>
      </div>
    <div class="row">
        <a href="{{route('stocks.list')}}" class="btn btn-primary">Vérifier les stocks</a>
    </div>
</div>
@endsection
