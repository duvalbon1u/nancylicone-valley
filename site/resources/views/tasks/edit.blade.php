@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Nouvelle tâche</div>
        <div class="card-body">

            <form action="{{route('tasks.update', $task->id)}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="task_title" class='col-md-4 control-label'>Nom</label>
                        <input type="text" class="form-control" id="task_title" name="task_title" value="{{$task->task_title}}">
                    </div>
                    <div class="col-md-6">
                        <label for="description" class='col-md-4 control-label'>Description</label>
                        <textarea type="text" class="form-control" id="summernote" name="description">{{$task->description}}</textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="id_todo" class='col-md-4 control-label'>Todo-List</label>
                        <select class="form-control" id="id_todo" name="id_todo">
                            @foreach($tdl as $td)
                                <option value="{{ $td->id }}" selected>{{$td->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="id_equipement" class='col-md-4 control-label'>Equipement</label>
                        <select class="form-control" id="id_equipement" name="id_equipement">
                            <option value="0">Aucun équipement</option>
                            @foreach($equipements as $equipement)
                                <option value="{{$equipement->id }}">{{$equipement->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="materialChecked2" name="checked" value="{{$task->checked}}">
                            <label class="form-check-label" for="materialChecked2">Effectué</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Nouvelle tâche
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection

