@extends('default')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-tasks"></i>
            Gérer les stocks</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Nom</th>
                <th>Fait ?</th>
                <th>Todo-List</th>
                <th>Equipement</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{{$task->task_title}}}</td>
                        <td>@if($task->checked)
                                <i class="fas fa-check"></i>
                            @else
                                <i class="fas fa-times"></i>
                            @endif
                        </td>
                        <td>{{$task->id_todo}}</td>
                        <td>@if($task->id_equipement != 0)
                                <i class="fas fa-check"></i>
                            @else
                                <i class="fas fa-times"></i>
                            @endif
                        </td>
                        <td>
                            <a href="{{{ route('tasks.edit', $task) }}}" class="btn btn-primary">Editer</a>
                            <a href="{{{ route('tasks.edit', $task) }}}" class="btn btn-primary">Afficher</a>
                            <form action="{{{ route('tasks.destroy', $task) }}}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('tasks.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
