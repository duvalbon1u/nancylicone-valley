@extends('default')
@section('content')
<div class="container">
      <div class="row">
          <a class="col square d-flex nolink" href="{{route('planning.index')}}">
              <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme1'>
                  <h2>Planning</h2>
              </div>
          </a>
          <a class="col square d-flex nolink" href="{{route('todo-list.index')}}">
              <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme5'>
                  <h2>Préparer une expédition</h2>
              </div>
          </a>
          <a class="col square d-flex nolink" href="{{route('todo-list.index')}}">
              <div class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-theme3'>
                  <h2>Opérations et protocoles</h2>
              </div>
          </a>
      </div>
</div>
@endsection
