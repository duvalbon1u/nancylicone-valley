<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">
    <title>Erreur</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/erreur.css">
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/p5.js"></script>
    <script src="js/jeuErreur.js"></script>
    <script src="js/particule.js"></script>
    <script src="js/artifice.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="{{route('root')}}">Survival</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
             <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle dropbtn" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Upgrades</a>
                <div class="dropdown-menu dropdown-content">
                  <a class="dropdown-item" id="2fois">Double click (prix)</a>
                  <a class="dropdown-item" id="addAuto">+1 click auto (prix)</a>
                  <a class="dropdown-item" id="baissAuto">Baisser temps auto (prix)</a>
                </div>
              </li>
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Informations</a>
                <div class="dropdown-menu dropdown-content">
                  <a class="dropdown-item" id="click">Multiplicateur click : (val)</a>
                  <a class="dropdown-item" id="clicka">Gain auto : (val)</a>
                  <a class="dropdown-item" id="clickt">Temps auto : (val)</a>
                </div>
              </li>
            <li class="nav-item active">
                <a class="nav-link" href="#">ERROR 404 : NOT FOUND<span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row" id ="centre">
    <script src="js/erreurP5.js"></script>
</div>


<footer class="footer">
    <div class="container">
        <div class="col-align-self-end col-md-4 offset-md-4">
            <p id="score">Score : 0</p>
        </div>
    </div>
</footer>
</body>
</html>
