let t = -1000;
var t2=-1000;
var xDebut;
var yDebut;
var x;
var y;
var varDirecX = 0.5;
var varDirecY = 1;
var imgTir;
var imgBoule;
var imgFond;
var imgDispa;
var imgLama;
var state = 1;
var dimension = 120;
var artifices = [];
var gravite;
var colors = [];
var lastClick = 0;
var auto = true;

var cosines = {}, sinuses = {};

function setup() {
    xDebut = (displayWidth/3)/2;
    yDebut = (displayHeight/1.5)/2+60;
    x=xDebut;
    y=yDebut;
    var canvas = createCanvas(displayWidth/3, displayHeight/1.5);
    gravite = createVector(0, 0.06);
    colors = [
        color(255, 120, 0), // "Orange"
        color(0, 255, 120), //"Vert"
        color(120, 0, 255), //"Violet"
        color(120, 120, 255), //"Bleu ciel"
        color(255, 120, 120), //"Rose pâle"
        color(120, 255, 120), //"Vert clair"
        color(255, 255, 120), //"Jaune"
        color(255, 120, 255), //"Rose"
        color(120, 255, 255), //"Vert turquoise"
    ];

    for (let i = 0; i <= TWO_PI + 0.5; i += 0.1)
    {
        let a = round(i * 10) / 10;
        cosines[a] = cos(a);
        sinuses[a] = sin(a);
    }
    canvas.parent('centre');
    imgTir = loadImage("img/pan.png");
    imgBoule= loadImage("img/vivotant.png");
    imgFond = loadImage("img/FondDesert.png");
    imgDispa = loadImage("img/nuage.png");
    imgLama = loadImage("img/lama.png")
}

function draw() {
    clear();
    fill(255, 204, 0);
    image(imgFond,0,0,displayWidth/3,displayHeight/1.5);
    image(imgBoule,x-dimension/2,y-dimension/2,dimension,dimension);
    x+=varDirecX;
    y+=varDirecY;
    if(x > xDebut+20 || x < xDebut -20){
        varDirecX*=-1;
    }
    if(y > yDebut+20 || y < yDebut -20){
        varDirecY*=-1;
    }
    if(millis() - t < 100){
        if(state==1){
            image(imgDispa, mouseX-imgDispa.width/20+20, mouseY-imgDispa.height/16+5,imgDispa.width/14,imgDispa.width/14);
        }
        else{
            image(imgTir, mouseX-imgTir.width/8, mouseY-imgTir.height/8,imgTir.width/4,imgTir.width/4);
        }
    }
    if(millis() - t2 < 100){
        image(imgTir, x-dimension/2,y-dimension/2,imgTir.width/4,imgTir.width/4);
    }
    if (tlama > 1000*60){
        lama();
        if(mousePressed()) && (mouseX >= presslamaX) && (mouseX <= presslamaX+imgLama.width) && (mouseY >= pressLamaY) && (mouseY <= pressLamaY+imgLama.height)
            artifices.push(new Artifice(mouseX, mouseY, random(colors), 100))
            addScoreLama();
        }
        
    }
    if (auto)
    {
        if (!(frameCount % 20))
            artifices.push(new Artifice(random(width), height, random(colors), 100));
    }
    else if (frameCount - lastClick > 300)
        auto = true;

    for (let i = artifices.length - 1; i >=0; i--) {
        let a = artifices[i];

        a.appliquerForce(gravite);
        if (a.update())
            a.draw();
        else
            artifices.splice(i, 1);
    }
}
     
function mousePressed(){
    var d = dist(mouseX, mouseY, x, y);
    if(d<dimension/2){
        state++;
        dimension-=4;
        if(state==20){
            state=1;
            dimension=100;
        }
        t = millis();
        addScore();
    }
    artifices.push(new Artifice(mouseX, height, random(colors), 100));
    lastClick = frameCount;
    auto = false;
    return false;
}

function cliqueAuto(){
    t2 = millis();
}



