
@extends('default')
@section('title')
    Excursion
@endsection
@section('content')
    <style type="text/css">
        #map { /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
            height: 400px;
        }
    </style>
    <div class="container">
        <div class='alert alert-info w-100 text-center content'>
        </div>
        <div class="row">

            <div class="col-10">
                <div id="all">

                    <div id="map"></div>

                </div>
            </div>
            <div class="col-2">
                <h1>Vent:</h1>
                <img src='/img/fleche.png' id='fleche'>
                <input type="button" class="btn btn-success"value="APPEL API METEO" onclick="buttonClickGET()"/><p id="zone_meteo">Météo non initialisée</p>
                <hr>

                <details>
                    <summary>Géolocalisation :</summary>
                    <p id="long">Longitude : null</p>
                    <p id="lat">Latitude : null</p>
                </details>
                <hr>
                <button class="btn btn-primary" onclick="refresh()">Recentrer</button>
            </div>
        </div>
    </div>

    <script>
        var botmanWidget = {
            frameEndpoint: 'chatbot/chat.html',
            introMessage: 'Bonjour, je suis votre compagnon de voyage',
            chatServer : 'chat.php',
            title: 'My Chatbot',
            mainColor: '#456765',
            bubbleBackground: '#ff76f4',
            aboutText: '',
            bubbleAvatarUrl: '',
        };
    </script>
    <script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
    <script src='/js/gyronorm.js'></script>
    <script src='/js/life.js'></script>
    <script>
    init_gn();
    </script>


@endsection
