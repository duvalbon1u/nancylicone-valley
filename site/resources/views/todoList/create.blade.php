@extends('default')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">Créer une Todo-list</div>
        <div class="card-body">

            <form action="{{route('todo-list.store')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="name" class='col-md-4 control-label'>Titre</label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                    <div class="col-md-6">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="materialChecked2" name="check">
                            <label class="form-check-label" for="materialChecked2">Effectué</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Nouvelle Todo-list</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
