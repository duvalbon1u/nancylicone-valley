@extends('default')

@section('content')
    <div class='container'>
        <h1 class="text-center mb-4">Opération {{ $tdl->title }}</h1>

        <div class='row'>
            <ol class='w-100 m-0'>
                @foreach($tdl->tasks()->get() as $task)
                    <li class ='mb-4'>
                        <div class='col-12 row'>
                            <div class='col-9'>
                                <h3>{{$task->task_title}}</h3>
                                <button class="btn btn-sm btn-outline-secondary mx-5 my-2" type="button" data-toggle="collapse" data-target="#desc-{{$task->id}}">
                                Afficher plus
                                  </button>
                                <div class="collapse" id="desc-{{$task->id}}">
                                  <div class="card card-body">{{$task->description}}</div>
                                </div>
                            </div>
                            <div class='col-3'>
                                <input type="checkbox" class="form-check-input checkbox-task" data-id="{{$task->id}}" name="check" {{ $task->checked ? " checked" : ""}}>
                            </div>
                        </div >
                    </li>
                @endforeach
            </ol>
        </div>
    </div>
@endsection
