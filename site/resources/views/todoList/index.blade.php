@extends('default')

@section('content')
    <h1 class="text-center mb-4">Résumé de toutes les opérations et protocoles disponibles</h1>

    <div class="row">
        @foreach ($tdl as $td)
            <div class='col-6 col-md-3 square d-flex p-1'>
                <a href='{{ route('todo-list.show', ['id' => $td->id]) }}' class='w-100 text-center m-0 p-1 d-flex align-items-center justify-content-center bg-{{$td->color}} nolink'>
                    <h3>{{$td->title}}</h3>
                </a>
            </div>
        @endforeach
    </div>
@endsection