@extends('default')

@section('content')
    <h1>Editer</h1>

    <div class="card card-default">
        <div class="card-header">Modifier une Todo-list</div>
        <div class="card-body">

            <form action="{{route('todo-list.update', ['id' => $tdl->id])}}" method="POST">
                @csrf
                @method('PUT')
                
                <div class="form-group">

                    <div class="col-md-6">
                        <label for="name" class='col-md-4 control-label'>Titre</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{$tdl->title}}">
                    </div>
                    <div class="col-md-6">
                        <div class="form-check">
                            
                            @if ($tdl->checked == 1)
                            <input type="checkbox" class="form-check-input" id="materialChecked2" name="check" checked="checked">
                            @else
                            <input type="checkbox" class="form-check-input" id="materialChecked2" name="check">
                            @endif
                            <label class="form-check-label" for="materialChecked2 checked">Effectué</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
