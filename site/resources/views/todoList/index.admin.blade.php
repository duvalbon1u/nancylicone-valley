@extends('default')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-list-ul"></i>
            Gérer les Todo-list</div>
        <div class="card-body">
            <table id="sorted_table" class="table table-striped">
                <thead>
                <th>Nom</th>
                <th>Fini ?</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($tdl as $td)
                    <tr>
                        <td>{{{$td->title}}}</td>
                        <td>@if($td->checked)
                                <i class="fas fa-check"></i>
                            @else
                                <i class="fas fa-times"></i>
                            @endif
                        </td>
                        <td>
                            <a href="{{{ route('todo-list.edit', $td) }}}" class="btn btn-primary">Editer</a>
                            <a href="{{{ route('todo-list.show', $td) }}}" class="btn btn-primary">Afficher</a>
                            <form action="{{{ route('todo-list.destroy', $td) }}}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer"><a href="{{ route('todo-list.create') }}" class="btn btn-success">Nouveau</a></div>
    </div>

@endsection
