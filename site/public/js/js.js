var longitude = document.getElementById('long');
var latitude = document.getElementById('lat');
var lat=0;
var lon=0;
var macarte = L.map('map');
var urlVaisseau = '/img/vaisseau.png';
var urldest = '/img/destination.png';

navigator.geolocation.watchPosition(locationUpdate, locationUpdateFail, {
    enableHighAccuracy: false,
    maximumAge: 30000,
    timeout: 27000
  });


function initMap() {
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
            macarte.setView([lat, lon], 11);
            // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
            L.tileLayer('https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=7d4e2704e8d3488fbde36feeac5ddb6b', {
                // Il est toujours bien de laisser le lien vers la source des données
                attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                minZoom: 1,
                maxZoom: 20
            }
          ).addTo(macarte);
          var position = L.icon({
			         iconUrl: urlVaisseau,
			         iconSize: [50, 50],
			         iconAnchor: [25, 50],
			         popupAnchor: [0, -50],
		      });

          var marker = L.marker([lat, lon], { icon: position }).addTo(macarte);
          marker.bindPopup("Ma position");
        }

var t = document.getElementById("all");
L.DomEvent.on(t,"click",function(e){
let latLong = macarte.layerPointToLatLng(L.point(e.layerX, e.layerY));
var destination = L.icon({
     iconUrl: urldest,
     iconSize: [50, 50],
     iconAnchor: [30, 30],
     popupAnchor: [0, -50],
});
var marker = L.marker(latLong, { icon: destination }).addTo(macarte);
marker.bindPopup("Ma destination");
});
function locationUpdate(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    latitude.textContent ="Latitude : "  + position.coords.latitude;
    longitude.textContent ="Longitude : "+ position.coords.longitude;
    initMap();
/**
    positionLat.textContent = decimalToSexagesimal(positionCurrent.lat, "lat");
    positionLng.textContent = decimalToSexagesimal(positionCurrent.lng, "lng");**/
  }


function locationUpdateFail(error) {
    lat.textContent = "Latitude : n/a";
    long.textContent ="Longitude : n/a";
    console.log("location fail: ", error);
  }

  function refresh() {
    if(macarte.getCenter().lat!==lat || macarte.getCenter().lng!==lon)
      initMap();
  }
