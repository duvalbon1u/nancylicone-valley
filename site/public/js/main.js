

$(".checkbox-task").change(e => {
    let elem = $(e.delegateTarget);
    console.log(elem.is(":checked"));
    console.log(elem.is(":checked") ? "on" : null);
    $.ajax("/tasks/" + elem.data('id'), {
        method: "PUT",
        data : {
            id : elem.data('id'),
            checked : elem.is(":checked") ? "on" : null,
            _token:$('meta[name="csrf-token"]').attr('content')
        }
    });
});
