class Particule {

    constructor(x, y, xVit, yVit, couleur, ttl = -1) {
        this.pos = createVector(x, y);
        this.prevPos = createVector(x, y);
        this.vitesse = createVector(xVit, yVit);
        this.acc = createVector(0, 0);
        this.couleur = couleur;
        this.weight = 5;
        this.ttl = ttl;
        this.sparkle = false;
    }

    appliquerForce(vec) {
        this.acc.add(p5.Vector.mult(vec, this.weight));
    }

    update() {
        if (!this.updateTtl())
            return false;

        this.vitesse.add(this.acc);
        this.pos.add(this.vitesse);
        this.acc = createVector(0, 0);

        return true;
    }

    draw() {
        if (this.checkTtl()) {
            strokeWeight(this.weight);

            let beginLerp = 20;
            let coeff = this.ttl > beginLerp || this.ttl === -1 ? 0 : map(this.ttl, 0, beginLerp, 1, 0);

            let r = lerp(red(this.couleur), 0, coeff);
            let g = lerp(green(this.couleur), 0, coeff);
            let b = lerp(blue(this.couleur), 0, coeff);

            stroke(floor(r), floor(g), floor(b));
            noFill();

            point(this.pos.x, this.pos.y);

            if (this.sparkle)
            {
                stroke(this.couleur);
                 let start = frameCount % 8 > 4 ? 1 : 0;
                for (let i = start; i < 8; i++)
                {
                    let a = floor(i / 8 * TWO_PI * 10) /10;
                    if ((i - start) % 2)
                    {
                        stroke(this.couleur);
                        point(this.pos.x + cosines[a] * random(2,8), this.pos.y + sinuses[a] * random(2,8));
                    }
                /*    else
                    {
                        stroke(50);
                        point(this.prevPos.x + cosines[a] * 5, this.prevPos.y + sinuses[a] * 5);
                    }*/
                }
            }
            this.prevPos.x = this.pos.x;
            this.prevPos.y = this.pos.y;
        }
    }

    updateTtl()
    {
        if (this.ttl === -1)
            return true;
        else if (!this.ttl)
        {
            if (this.sparkle)
                return false;
            else
            {
                this.sparkle = true;
                this.ttl = 16;
            }
        }
        this.ttl--;
        return true;
    }

    checkTtl()
    {
        return this.ttl === -1 || this.ttl != 0;
    }
}
