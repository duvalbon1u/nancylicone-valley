
class Artifice extends Particule {

    constructor(x, y, couleur) {
        let yMin = height / 90, yMax = height / 42;
        super(x, y, 0, -random(yMin, yMax), couleur);
        this.weight = 3;
        this.particles = [];
    }

    update() {
        if (this.particles.length)
        {
            let end = true;
            for (let p of this.particles)
            {
                p.vitesse.mult(0.95);
                p.appliquerForce(gravite);
                if (p.update())
                    end = false;
            }
            return !end;
        }
        else {
            super.update();

            if (this.vitesse.y >= 0 ) {
                for (let i = 0; i < TWO_PI; i += random(.2)) {
                    let a = round(i * 10) / 10;
                    let vitesse = createVector(cosines[a], sinuses[a]);
                    vitesse.mult(random(3));
                    this.particles.push(new Particule(this.pos.x, this.pos.y, vitesse.x, vitesse.y, this.couleur, floor(random(80, 100))));
                }
            }
        }
        return true;
    }

    draw() {
        if (this.particles.length) {
            for (let p of this.particles)
                p.draw();
        } else
            super.draw();
    }


}
