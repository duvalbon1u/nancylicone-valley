var gn;
var alpha, beta, gamma;
var time = new Date().getTime();
var sensibility = 5;
var brusque = 20;
var mouvBrusque = false;
var periode = 10000 // en milliseconde !
function init_gn() {

  gn = new GyroNorm();

  gn.init({logger: console}).then(function() {
    var isAvailable = gn.isAvailable();
    start_gn();
  }).catch(function(e){

    console.log(e);

  });
}

function stop_gn() {
  gn.stop();
}

function start_gn() {
  gn.start(gnCallBack);
}

function gnCallBack(data) {
    alpha_new = data.do.alpha;
    beta_new = data.do.beta;
    gamma_new = data.do.gamma;

    if(mouvBrusque){
        time_new = new Date().getTime();

        console.log(mouvBrusque);
        if(time_new-time > periode/2 && alpha-alpha_new < sensibility && alpha-alpha_new > -sensibility && beta-beta_new < sensibility && beta-beta_new > -sensibility && gamma-gamma_new < sensibility && gamma-gamma_new > -sensibility){
            $('.content').text("Alerte pas de mouvement après mouvement brusque !");
        } else if(time_new-time > periode/2){
            mouvBrusque = false;
            time = time_new;
        }

    } else if(alpha-alpha_new < sensibility && alpha-alpha_new > -sensibility && beta-beta_new < sensibility && beta-beta_new > -sensibility && gamma-gamma_new < sensibility && gamma-gamma_new > -sensibility){
        time_new = new Date().getTime();

        if(time_new-time > periode){
            $('.content').text("Alerte pas de mouvement !");
        } else{
            $('.content').text(data.do.alpha + '   ' + data.do.beta + '   ' + data.do.gamma);
        }
    } else if(alpha-alpha_new > brusque || alpha-alpha_new < -brusque || beta-beta_new > brusque || beta-beta_new < -brusque || gamma-gamma_new > brusque || gamma-gamma_new < -brusque){
        time = new Date().getTime();
        alpha = alpha_new;
        beta = beta_new;
        gamma = gamma_new;
        $('.content').text('Mouvement brusque !');
        mouvBrusque = true;

    } else{
        time = new Date().getTime();
        alpha = alpha_new;
        beta = beta_new;
        gamma = gamma_new;
        $('.content').text(data.do.alpha + '   ' + data.do.beta + '   ' + data.do.gamma);
    }
}

function norm_gn() {
  gn.normalizeGravity(true);
}

function org_gn() {
  gn.normalizeGravity(false);
}

function set_head_gn() {
  gn.setHeadDirection();
}
