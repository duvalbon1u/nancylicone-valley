<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    protected $table = "planning";
    protected $primaryKey="id";
}
