<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Data extends Model
{
    protected $table="data";
    protected $primaryKey="id";
    /**
     * give the number of hour between the date contained
     * in this object and the current date	
     * @param $selector give the date to select
     * @return number of hour between the date contained
     * 		   in this object and the current date
     */
    private function hour_number($selector){
    	$date=null;

    	switch($selector){
    		case 1:
    			$date=$this->last_time_sleep;
    		break;

    		case 2:
    			$date=$this->last_time_eat;
    		break;

    		case 3:
    			$date=$this->last_time_drink;
    		break;
    	}

    	if($date!=null){
    		$current_date=Carbon::now(1);
    		$last_date=Carbon::createFromTimestamp($date);

    		return ($current_date->diffInHours($last_date));
    	}else
    		return -1;
    }
    /**
     * give the number of hour between the last nap
     * and the current date	
     * @return number of hour between the last nap and the current date
     */
    public function hour_number_sleep(){
    	return $this->hour_number(1);
    }
    /**
     * give the number of hour between the last time the 
     * explorer had eaten and the current date	
     * @return number of hour between the date contained
     * 		   in this object and the current date
     */
    public function hour_number_eat(){
    	return $this->hour_number(2);
    }
    /**
     * give the number of hour between the last time 
     * the explorer had drunk and the current date	
     * @return number of hour between the date contained
     * 		   in this object and the current date
     */
    public function hour_number_drink(){
    	return $this->hour_number(3);
    }
}
