<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    protected $table='tasks';
    protected $primaryKey='id';
    protected $fillable = ['task_title','checked', 'description','id_todo','id_equipement'];
 	
}
