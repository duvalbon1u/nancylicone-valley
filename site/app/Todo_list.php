<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo_list extends Model
{
    protected $table = "todo_lists";
    protected $primaryKey = "id";

    public function tasks(){
    	return $this->hasMany('App\Tasks', 'id_todo');
    }

    public function plannings(){
    	return $this->hasMany('App\Planning');
    }
}
