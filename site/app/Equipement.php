<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipement extends Model
{
    protected $table = "equipements";
    protected $primaryKey = "id";
}
