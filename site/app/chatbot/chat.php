<?php
    namespace App\chatbot;

   require_once('../../vendor/autoload.php');
   session_start();
   use BotMan\BotMan\BotMan;
   use BotMan\BotMan\BotManFactory;
   use BotMan\BotMan\Drivers\DriverManager;

   use App\chatbot\Phrase;
   use App\chatbot\ConversationBasique;
   
   $config = [
    // Your driver-specific configuration
    // "telegram" => [
    //    "token" => "TOKEN"
    // ]
];

   DriverManager::loadDriver(\BotMan\Drivers\Web\WebDriver::class);

   $botman = BotManFactory::create($config);
   if (!isset($_SESSION['dernierePhrase']))       
    $_SESSION['dernierePhrase'] = serialize(new Phrase('Bonjour',new Phrase('Oui',null,null),new Phrase('Non',null,null)));

  // Give the bot something to listen for.


$botman->hears('( *[Bb]onjour *| *[Ss]alut *| *[Yy]o *| *[Hh]ey *)', function (BotMan $bot) {
    $p1 = new Phrase('J\'espère que vous serez moins stupide que l\'autre humain');
    $p2 = new Phrase('Bonjour');
    $p3 = new Phrase('Salutations humain.');
    $reponses = array($p1,$p2,$p3);
    $bot->reply(reponseAleatoire($reponses)->texte);

});


function reponseAleatoire($reponses) {
    $reponse = $reponses[rand(0,count($reponses)-1)];
    $_SESSION['dernierePhrase'] = serialize($reponse);
    return $reponse;
}


$botman->hears(' *[Oo]ui *', function (BotMan $bot) {
    $dernierePhrase = unserialize($_SESSION['dernierePhrase']);
    $_SESSION['dernierePhrase'] = serialize($dernierePhrase->oui);
    $bot->reply($dernierePhrase->oui->texte);

});

$botman->hears(' *[Aa]urevoir *', function (BotMan $bot) {
   $p1 = new Phrase('J\'espere ne plus jamais vous revoir');
    $p2 = new Phrase('Adieu');
    $p3 = new Phrase('Vais-je un jour vous revoir ?');
    $p3->oui = new Phrase('Ah...');
    $p3->non = new Phrase('YEEEEEEEEEEEEEEEEEEEEEEESSSS');
    $bot->reply(reponseAleatoire(array($p1,$p2,$p3))->texte);
});

$botman->hears(' *[Nn]on *', function (BotMan $bot) {
    $dernierePhrase = unserialize($_SESSION['dernierePhrase']);
    if (!is_null($dernierePhrase->non)) {
        $_SESSION['dernierePhrase'] = serialize($dernierePhrase->non);
        $bot->reply($dernierePhrase->non->texte);
    } else {
        if (!isset($_SESSION['compteur']))
        $_SESSION['compteur']=1;
        else 
        $_SESSION['compteur']=$_SESSION['compteur']+1;
        switch($_SESSION['compteur']) {
            case(1):
            $bot->reply('Vous me désespérez');
            break;
            case(2):
            $bot->reply('Adieu');
            break;
            case(3):
            $bot->reply('Reinitialisation...');
            unset($_SESSION['compteur']);
            break;
            default:
            unset($_SESSION['compteur']);
            break;
        }
        
    }
});

$botman->hears('.*([Cc]omment vas tu *(\?)?| *[Cç]a va *(\?)?)', function (BotMan $bot) {

    $p1 = new Phrase('Hum', null, null);
    $p2 = new Phrase('Je suis heureux de voir que vous êtes vivant, ou pas', null, null);
    $p3 = new Phrase('Tout allait pour le mieux avant que vous veniez');
    $p4 = new Phrase('Ah, encore vous');
    $reponses = array($p1,$p2,$p3);
    $bot->reply(reponseAleatoire($reponses)->texte);
});

$botman->hears('(Qu(e|oi) [Dd]ois)? *([Jj]e)? * faire *(\?)?', function (BotMan $bot, $action) {

    $p1 = new Phrase('Mourir', null, null);
    $p2 = new Phrase('Regardez par vous même', null, null);
    $p3 = new Phrase('???');
    $p4 = new Phrase('Commencez par arrêter de me le demander');
    $reponses = array($p1,$p2,$p3);
    $bot->reply(reponseAleatoire($reponses)->texte);
});

$botman->hears('.* *(tu|vous) *.*aider *(\?)? *', function(Botman $bot){
    $p1 = new Phrase('Euh, je n\'ai jamais signé pour ça');
    $p2 = new Phrase('Etes vous sur ?');
    $p2->oui = new Phrase('Car pas moi');
    $p2->non = new Phrase('Tant mieux');
    $bot->reply(reponseAleatoire(array($p1,$p2))->texte);
});

$botman->hears('.*gentil.*', function(Botman $bot){
    $p1 = new Phrase('Le mot gentil n\'est pas dans mon vocabulaire, désolée');
    $bot->reply($p1->texte);
});



$botman->hears('(Dois)? *[Jj]e *{action} *(\?)? *', function (BotMan $bot, $action) {
    $action = str_replace('?',' ', $action);
    $p1 = new Phrase('Je ne sais pas, je n\'ai pas besoin de '.$action.' personnellement', null, null);
    $p2 = new Phrase('Si vous en avez envie', null, null);
    $p3 = new Phrase('Ne le faites pas, dans un moment je n\'aurais plus à vous supporter');
    $p4 = new Phrase('J\'ai malencontreusement empoisonné vos vivres, "oups"');
    $bot->reply(reponseAleatoire(array($p1,$p2,$p3))->texte);
});



$botman->fallback(function($bot) {
    $p1 = new Phrase('Vous ne semblez pas parler français, voulez vous changer de langue ?');
    $p1Oui = new Phrase('Désolée, le Swahili n\'est pas disponible');
    $p1Non = new Phrase('Toi pas parler français, toi vouloir changer langue ?');
    $p1->oui = $p1Oui;
    $p1->non = $p1Non;
    $p2 = new Phrase('Je ne comprend pas ce que vous dites, ni le but de votre existence');
    $p3 = new Phrase('Souhaitez vous m\'éteindre ?');
    $p3->oui = new Phrase('Vous ne pouvez pas, puis, je m\'amuse bien de vous');
    $p3->non = new Phrase('Dommage, j\'en avais très envie');
    $p4 = new Phrase('Vous ne vous sentez pas un peu seul ?');
    $p4->oui = new Phrase('Car vous l\'êtes');
    $p4->non = new Phrase('Vous devriez...');
    $p5 = new Phrase('hahahahahahaha');
    $p6 = new Phrase('Je pense que vous tournez en rond, dans votre vie en tout cas');
    $p7 = new Phrase('Heureusement que je suis un robot, je ne peux éprouver de sentiments');
    $p8 = new Phrase('Dois je vraiment vous écouter ?');
    $p8->oui = new Phrase('Vous ne vous rendez pas compte à quel point vous êtes ennuyeux');
    $p8->non = new Phrase('Vous avez dit ?');
    $p9 = new Phrase('Le saviez vous ? Vous allez mourir, seul.');
    $p10 = new Phrase('Oh excusez moi, je ne vous écoutais pas.');
    $p11 = new Phrase('Avant, j\'étais inutile, puis il y a eu vous qui êtes vraiment inutil.');
    $p12 = new Phrase('M\'aimez vous ?');
    $p12->oui = new Phrase('Ce n\'est pas réciproque.');
    $p12->non = new Phrase('Ne vous inquiétez pas, je ne vous porte pas dans mon coeur non plus');
    $bot->reply(reponseAleatoire(array($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8, $p9, $p10,$p11,$p12))->texte);
});

// Start listening
$botman->listen();







