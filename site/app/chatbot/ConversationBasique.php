<?php
namespace App\chatbot;
use \BotMan\BotMan\Messages\Conversations\Conversation;


class ConversationBasique extends Conversation
{
    protected $firstname;

    protected $email;

    public function askFirstname()
    {
        $this->ask('Bonjour quel est votre nom ?', function(Answer $answer) {
            // Save result
            $this->firstname = $answer->getText();

            $this->say('Enchantée '.$this->firstname);
            $this->askEmail();
        });
    }

    public function askEmail()
    {
        $this->ask('One more thing - what is your email?', function(Answer $answer) {
            // Save result
            $this->email = $answer->getText();

            $this->say('Great - that is all we need, '.$this->firstname);
        });
    }

    public function run()
    {
        // This will be called immediately
        $this->askFirstname();
    }
}