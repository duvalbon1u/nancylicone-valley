<?php

namespace App\chatbot;

class Phrase {

    public $texte, $oui, $non;

    public function __construct($t) {
        $this->texte = $t;
        $this->oui = null;
        $this->non = null;
    }

}