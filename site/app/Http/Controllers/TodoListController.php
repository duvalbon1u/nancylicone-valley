<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Todo_list;

class TodoListController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tdl = Todo_list::all();
        $colors = ['theme1', 'theme2', 'theme3', "theme4", 'theme5'];
        foreach($tdl as $td)
        {
            $td["color"] = $colors[array_rand($colors)];
        }
        return view("todoList.index", compact("tdl"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("todoList.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|String',
            'check' => 'filled',
        ]);

        $tdl = new Todo_list();
        $tdl->title = $request->title;

        if(isset($request->check)){
            $tdl->checked = true;
        } else {
            $tdl->checked = false;
        }

        $tdl->save();

        $request->session()->flash('msg', 'L\'élément a bien été ajouté !');


        return redirect()->route("todo-list.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tdl = Todo_list::find($id);

        return view("todoList.show", compact('tdl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tdl = Todo_list::find($id);

        return view("todoList.edit", compact('tdl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'String',
            'check' => 'filled',
        ]);

        $tdl = Todo_list::find($id);
        if (isset($request->title))
            $tdl->title = $request->title;

        if(isset($request->check)){
            $tdl->checked = true;
        } else {
            $tdl->checked = false;
        }

        $tdl->save();


        $request->session()->flash('msg', 'L\'élément a bien été modifié !');

        return redirect()->route("todo-list.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tdl = Todo_list::find($id);
        $tdl->delete();

        $request->session()->flash('msg', 'L\'élément a bien été supprimé !');

        return redirect()->route("todo-list.index");
    }
}
