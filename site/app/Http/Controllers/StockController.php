<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Type;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stocks = Stock::all();
        $types = Type::all();
        return view('stocks.index', compact("stocks","types"));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $stocks = Stock::all();
        $types = Type::all();
        return view('stocks/list', compact("stocks","types"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        return view('stocks.create',compact("types"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'name' => 'required|unique:stocks|max:255',
            'quantity' => 'required',
            'id_type' => 'required',
            'unity' => 'required',
            ]);
        Stock::create($validatedData);
        $stocks = Stock::all();
        $types = Type::all();
        return view('stocks.index', compact("stocks","types"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stock = Stock::find($id);
        return view('stocks.show', compact("stock"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stock = Stock::find($id);
        $types = Type::all();
        return view('stocks.edit', compact("stock","types"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'quantity' => 'required',
            'id_type' => 'required',
            'unity' => 'required',
        ]);
        Stock::find($id)->update($validatedData);
        $stocks = Stock::all();
        $types = Type::all();
        return view('stocks.index', compact("stocks","types"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock=Stock::find($id);
        $stock->delete();
        $stocks = Stock::all();
        $types = Type::all();
        return view('stocks.index', compact("stocks","types"));
    }


}
