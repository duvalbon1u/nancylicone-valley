<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;

class AlertController extends Controller
{
    public function log(Request $request)
    {
        $data = $request->validate([
            "type" => "required|string",
            "message" => "required|string"
        ]);

        $l = new Log();
        $l->type = $data->type;
        $l->message = $data->message;
        $l->save();

        return $request->json(array("code" => 200));       
    }
}
