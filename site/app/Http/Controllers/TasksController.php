<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tasks;
use App\Todo_list;
use App\Equipement;

class TasksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks=Tasks::all();
        $tdl = Todo_list::all();
        return view('tasks.index',compact('tasks',"tdl"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tdl = Todo_list::all();
        $equipements = Equipement::all();
        return view("tasks.create",compact("tdl","equipements"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'task_title' => 'required|String|max:255',
            'checked' => 'filled',
            'description' => 'required|String',
            'id_todo' => 'required|Integer',
            'id_equipement' => 'Integer'
        ]);

        $task = new Tasks();
        $task->task_title = $request->task_title;
        if(isset($request->checked)){
            $task->checked = true;
        } else {
            $task->checked = false;
        }
        $task->description = $request->description;
        $task->id_todo = $request->id_todo;
        if($request->id_equipement = 0){
        $task->id_equipement = Nil;
        } else {
            $task->id_equipement = $request->id_equipement;
        }


        $task->save();

        $tasks=Tasks::all();
        $tdl = Todo_list::all();
        return view('tasks.index',compact('tasks',"tdl"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task=Tasks::find($id);
        return view('tasks.show',compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $task=Tasks::find($id);
        $tdl = Todo_list::all();
        $equipements = Equipement::all();
        return view("tasks.edit",compact("task","tdl","equipements"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'task_title' => 'String|max:255',
            'checked' => '',
            'description' => 'String',
            'id_todo' => 'Integer',
            'id_equipement' => 'Integer'
        ]);

        $task = Tasks::find($id);
        if(isset($$request->task_title))
            $task->task_title = $request->task_title;
        if(isset($request->checked)){
            $task->checked = true;
        } else {
            $task->checked = false;
        }
        if(isset($$request->description))
            $task->description = $request->description;
        if(isset($$request->id_todo))
            $task->id_todo = $request->id_todo;
        if($request->id_equipement = 0){
            $request->id_equipement = null;
        } else {
            $task->id_equipement = $request->id_equipement;
        }
        //dd($task);
        $task->save();
        $tasks=Tasks::all();
        $tdl = Todo_list::all();
        return view('tasks.index',compact('tasks',"tdl"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task=Tasks::find($id);
        $task->delete();
        return redirect()->route('tasks.index');
    }
}
