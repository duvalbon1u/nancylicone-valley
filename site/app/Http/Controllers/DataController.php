<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Data;
use Carbon\Carbon;

class DataController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas=Data::all();
        return view('data.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datenow=Carbon::now();
        return view('data.create',compact("datenow"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'weight' => 'required|min:1',
            'height' => 'required|min:1',
            'beat' => 'required|min:0',
            'last_time_sleep' => 'date|required',
            'last_time_eat' => 'date|required',
            'last_time_drink' => 'date|required'
        ]);
        
        $data = new Data();
        $data->weight = $request->weight;
        $data->height = $request->height;
        $data->beat = $request->beat;
        $data->last_time_sleep = $request->last_time_sleep;
        $data->last_time_eat = $request->last_time_eat;
        $data->last_time_drink = $request->last_time_drink;

        $mess=$data->save() ? "successfull saving in database":"fail to save in database";
        
        $request->session()->flash("mess", $mess);

        $datas=Data::all();
        return view('data.index', compact('datas'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Data::all()->find($id);
        return view('data.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Data::find($id);
        return view('data.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result=$request->validate([
            'weight' => 'required|min:1',
            'height' => 'required|min:1',
            'beat' => 'required|min:0',
            'last_time_sleep' => 'required',
            'last_time_eat' => 'required',
            'last_time_drink' => 'required'
        ]);

        $data=Data::find($id);

        if(isset($result->weight))
            $data->weight=$result->weight;
        if(isset($result->height))
            $data->height=$result->height;
        if(isset($result->beat))
            $data->beat=$result->beat;
        if(isset($result->last_time_sleep))
            $data->last_time_sleep=$result->last_time_sleep;
        if(isset($result->last_time_eat))
            $data->last_time_eat=$result->last_time_eat;
        if(isset($result->last_time_drink))
            $data->last_time_drink=$result->last_time_drink;

        $data->save();
        $datas=Data::all();
        return view('data.index', compact('datas'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Data::all()->find($id);
        $mess=$data->delete();
        $datas=Data::all();
        return view('data.index', compact('datas'));
    }
}
