<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableauDeBordController extends Controller
{
    public function afficher()
    {
        return view("tableau-de-bord");
    }
}
