<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipement;

class EquipementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipements = Equipement::all();
        return view("equipements.index", compact("equipements"));
    }
    public function list()
    {
        $equipements = Equipement::all();
        return view("equipements.list", compact("equipements"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("equipements.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "required|string|min:5|max:255",
            "type" => ["required", Rule::in([Equipement::$types])]
        ]);

        $e = new Equipement();
        $e->name = $data->name;
        $e->type = $data->type;
        $e->save();

        return redirect()->route("equipements.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $equip = Equipement::find($id);
        return view("equipements.show", compact("equip"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipement = Equipement::find($id);
        return view("equipements.edit", compact("equipement"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            "carac" => "string",
        ]);

        $e = Equipement::find($id);

        $e->carac = $data['carac'];

        $e->save();
        $equipements = Equipement::all();
        return view("equipements.list", compact("equipements"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $e = Equipement::find($id);
        $e->delete();
        return redirect()->route("equipements.index");
    }
}
