<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Planning;
use App\Todo_list;

class PlanningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plannings=Planning::all();
        return view('planning.index',compact('plannings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tdls=Todo_list::all();
        return view('planning.create',compact('tdls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'horaire' => 'required|Time',
            'id_todo' => 'required|Integer'
        ]);

        $plan = new Planning();
        $plan->horaire = $request->horaire;
        $plan->id_todo = $request->id_todo;

        $mess=$plan->save() ? "successfull saving in database":"fail to save in database";

        $request->session()->flash("mess", $mess);

        return redirect()->route('planning.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $planning=Planning::find($id);
        return view('planning.show',compact('planning'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $planning=Planning::find($id);
        return view('planning.edit',compact('planning'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result=$request->validate([
            'horaire' => 'required|Time',
            'id_todo' => 'required|Integer'
        ]);

        $plan=Planning::find($id);

        if(isset($result->weight))
            $plan->horaire=$result->horaire;
        if(isset($result->height))
            $plan->id_todo=$result->id_todo;

        $plan->save();
        return view(route('planning.show',['id'=>$id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $planning=Planning::find($id);
        $mess=$planning->delete() ? "successfull suppressing":"fail in suppressing resource";
        $request->session()->flash('mess',$mess );
        return redirect()->route('data.index');
    }
}
